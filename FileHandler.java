import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FileHandler {
    /**
     * Reads a file in the following format:
     *
     *  M
     *  x x x
     *  x x x
     *  x x x
     *
     * Where M is the number of vectices, the following matrix has M
     * lines with M numbers (a MxM matrix).
     *
     * @param filename file to be read
     * @return a double-byte-array filled with the NxN matrix
     */
    public static FlowNetwork readFile(String filename) {
	Scanner scan = null;
	int m = 0; /* Number of rows x columns */

        try {
            scan = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.exit(1);
	}

	if (scan.hasNextInt()) {
	    m = scan.nextInt();
	} else {
	    System.err.println("Empty file: " + filename);
	}

	FlowNetwork network = new FlowNetwork(m);
	for (int i = 0; scan.hasNextInt(); i++) {
	    /* Read the network with capacities */
	    network.addEdge(i/m, i%m, scan.nextInt());
	}

	return network.runFordFulkersonAlgorithm();
    }

    /**
     * Writing to outputfile
     */
    public static void writeToFile(String filename, int maximumFlow,
				   String solution, String sourceSideOfCut,
				   int numberOfIncrementalSteps) {
	PrintWriter out = null;

	try {
	    out = new PrintWriter(new File(filename));
	    out.println(maximumFlow);
	    out.println(solution);
	    out.println(sourceSideOfCut);
	    out.println(numberOfIncrementalSteps);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.exit(1);
	} finally {
	    out.close();
	}



    }
}
