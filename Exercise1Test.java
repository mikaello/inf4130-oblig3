import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Exercise1Test {
    FlowNetwork network;

    Exercise1Test() {
	network = FileHandler.readFile("testfile.txt");
    }


    public void correctNumberOfVerticesInNetwork() {
	try {
	    assertEquals("Not correct number of vertices ", 5, network.getNumberOfVertices());
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- Number of vertices in network not implemented");
	}
    }

    public void correctNumberOfEdgesInNetwork() {
	try {
	    assertEquals("Not correct number of edges ", 8, network.getNumberOfEdges());
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- Number of edges in network not implemented");
	}
    }

    public void correctMaximumFlowInNetwork() {
	try {
	    assertEquals("Not correct maximum flow ", 4, network.getMaximumFlow());
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- Maximum flow in network not implemented");
	}
    }

    public void correctSourceSideOfCut() {
	try {
	    assertEquals("Not correct cut ", "1 2 4", network.getSourceSideOfCut());
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- Source side of cut not implemented");
	}
    }

    public void correctNumberOfIncrementalSteps() {
	try {
	    assertEquals("Not correct number of incremental steps ", 4,
			 network.getNumberOfIncrementalSteps());
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- Number of incremental steps not implemented");
	}
    }

    public void correctStringOutputFrowNetwork() {
	try {
	    String correctOutput = "4\n0 3 1 0 0\n0 0 1 2 0\n0 0 0 0 3\n0 0 1 0 1\n0 0 0 0 0\n1 2 4\n4";

	    String program = network.getMaximumFlow() + "\n";
	    program += network.getNetworkFlowInStringRepresentation(true) + "\n";
	    program += network.getSourceSideOfCut() + "\n";
	    program += network.getNumberOfIncrementalSteps();

	    assertEquals("Not correct string output ", correctOutput, program);
	} catch (UnsupportedOperationException e) {
	    System.err.println("--- String output from network not implemented");
	}
    }


}
