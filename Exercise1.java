/**
 * Created 2013/11/06 16:06:23
 * @author mikaello
 */
public class Exercise1 {
    public static void main(String[] args) {
        if (args.length == 0) {
            Exercise1Test test = new Exercise1Test();
            test.correctNumberOfVerticesInNetwork();
            test.correctNumberOfEdgesInNetwork();
            test.correctMaximumFlowInNetwork();
            test.correctSourceSideOfCut();
	    test.correctNumberOfIncrementalSteps();
            test.correctStringOutputFrowNetwork();
	    return;
        } else if (args.length != 2) {
            System.err.println("Usage: java Oblig2 <input> <output>");
            System.exit(1);
        }

        FlowNetwork network = FileHandler.readFile(args[0]);

	// Write solution to file
	FileHandler.writeToFile(args[1], network.getMaximumFlow(),
				network.getNetworkFlowInStringRepresentation(true),
				network.getSourceSideOfCut(),
				network.getNumberOfIncrementalSteps());
    }
}
