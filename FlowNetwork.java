import java.util.Set;
import java.util.TreeSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Formatter;
import java.util.HashSet;

/**
 * A flow network.
 *
 * All the edges from the start node is in indices
 * [0][x] where x goes from 0 to (size_of_network-1).
 *
 * The edges that goes into the sink is in indices
 * [x][length_of_network-1] where x goes from 0 to (size_of_network-1)
 */
public class FlowNetwork {
    /** A matrix containing the original flow network */
    private FlowEdge[][] network;

    /** A matrix containing the calculated max flow in this network */
    private FlowEdge[][] maximumFlowNetwork;

    /** All vertices in the source side of the cut */
    private boolean[] visitedVertex;

    /** A count of number of vertices (used for memoization) */
    private int numberOfVertices;

    /** A count of number of vertices (used for memoization) */
    private int numberOfEdges;

    /** Maximum flow in this network, calculated after Ford-Fulkerson algorithm */
    private int maximumFlow;

    /** Number of incremental steps (number of new paths found) */
    private int numberOfIncrementalSteps;

    /** A boolean that is set to true each time new vertices (edges) are added */
    private boolean updatedVertices = true;

    /** A boolean that is set to true each time new edges are added */
    private boolean updatedEdges = true;

    /** Differented formatting on output for different boardsizes */
    private boolean niceOutput = false;

    private final int SOURCE;
    private final int SINK;

    FlowNetwork(int dimension) {
        network = new FlowEdge[dimension][dimension];
	SOURCE = 0;
	SINK = dimension-1;
    }

    /**
     * Runs the Ford-Fulkerson algorithm on this FlowNetwork
     */
    public FlowNetwork runFordFulkersonAlgorithm() {
	maximumFlowNetwork = copyNetwork(network);
	maximumFlow = numberOfIncrementalSteps = 0;

	FlowEdge[] pathFrom = null;
	while ((pathFrom = getAugmentingPathUsingBreadthFirstSearch()) != null) {
	    FlowEdge e = null;

	    int bottleneckCapacity = Integer.MAX_VALUE;

	    for (int i = SINK; i != SOURCE; i = e.getOtherVertex(i)) {
		// Calculate bottleneck capacity
		e = pathFrom[i];
		bottleneckCapacity = Math.min(bottleneckCapacity, e.getRemainingCapasityToVertex(i));
	    }

	    for (int i = SINK; i != SOURCE; i = e.getOtherVertex(i)) {
		// Populate residual network
		e = pathFrom[i];
		e.addFlowToEdgeByVertex(i, bottleneckCapacity);
	    }

	    maximumFlow += bottleneckCapacity;
	}

	return this;
    }

    /**
     * Returns a augmenting path if this graph has an augmenting path form source
     * to sink. As explained for this class, the source vertex is
     * contained in [0][x] where x goes from 0 to (size_of_network-1),
     * and the sink is contained in [x][length_of_network-1] where x
     * goes from 0 to (size_of_network-1)
     * @return a augmenting path from source to sink
     */
    private FlowEdge[] getAugmentingPathUsingBreadthFirstSearch() {
        Queue<Integer> vertexQueue = new LinkedList<>();
	FlowEdge[] path = new FlowEdge[network.length];  // Vertex to edge path
        vertexQueue.add(0);

	visitedVertex = new boolean[network.length];
	visitedVertex[0] = true;                         // Source is always a visited vertex

        while (!vertexQueue.isEmpty()) {
	    int currentVertex = vertexQueue.poll();

	    for (FlowEdge e : getAdjacentEdgesToVertex(currentVertex)) {
		int possibleNextVertex = e.getOtherVertex(currentVertex);

		if (e.getRemainingCapasityToVertex(possibleNextVertex) > 0 && !visitedVertex[possibleNextVertex]) {
		    vertexQueue.add(possibleNextVertex);          // Add to queue
		    visitedVertex[possibleNextVertex] = true;     // Mark as visited
		    path[possibleNextVertex] = e;                 // Add to path

		    if (possibleNextVertex == SINK) {
			// We have found a successfull path
			numberOfIncrementalSteps++;
			return path;
		    }
		}
	    }
        }

	return null;
    }

    /**
     * Compute the maximum flow in this flow network using the
     * Ford-Fulkerson algorithm.
     */
    public int getMaximumFlow() {
	return maximumFlow;
    }

    /**
     * Numebr of incremental steps. That is the number of times a new
     * N(f) is constructed and found a path through it from the source
     * to the sink.
     */
    public int getNumberOfIncrementalSteps() {
	return numberOfIncrementalSteps;
    }

    /**
     * A line with the numbers of the vertices on the source side of a
     * cut with the capacity of the flow, in sorted order. The source
     * is included.
     *
     * Example:
     *   1 2 4
     */
    public String getSourceSideOfCut() {
	String cut = "";

	/*
	   Uses visitedVertex because after the FordFulkerson
	   algorithm has been run, and
	   getAugmentingPathUsingBreadthFirstSearch() has been run for
	   the last time, this boolean array will be populated with
	   all paths from source to a bottleneck, and that is the same
	   as the source side of the cut.
	 */

	for (int i = 0; i < visitedVertex.length; i++) {
	    if (visitedVertex[i]) {
		cut += (i+1) + " "; // Add one because index starts at zero
	    }
	}

	return cut.trim();
    }

    private Set<FlowEdge> getAdjacentEdgesToVertex(int vertex) {
	Set<FlowEdge> adjacentEdges = new TreeSet<>();

	for (int i = 0; i < network.length; i++) {
	    if (maximumFlowNetwork[vertex][i].isRealEdge() )
		adjacentEdges.add(maximumFlowNetwork[vertex][i]);

	    if (maximumFlowNetwork[i][vertex].isRealEdge())
		adjacentEdges.add(maximumFlowNetwork[i][vertex]);
	}

	return adjacentEdges;
    }

    /**
     * Copies a double array of FlowEdge's and returns that. This is a
     * deep copy.
     * @return a deep copy of the FlowEdge[][] given as parameter
     */
    public FlowEdge[][] copyNetwork(FlowEdge[][] f) {
        FlowEdge[][] copy = new FlowEdge[f.length][f[0].length];

        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[i].length; j++) {
                copy[i][j] = new FlowEdge(f[i][j]);
            }
        }

        return copy;
    }

    /**
     * Adds an edge to this graph. A new FlowEdge-object is created
     * and added to the double array. The boolean values
     * updatedVertices and updatedEdges are updated to reflect changes
     * in the graph (these are used for memoization in other methods).
     */
    public boolean addEdge(int originVertex, int destinationVertex, int capacity) {
        if (network[originVertex][destinationVertex] != null) {
            System.err.println("Vertex already exists: ");
            System.err.println(new FlowEdge(originVertex, destinationVertex, capacity).toString());
            return false;
        } else {
            network[originVertex][destinationVertex] = new FlowEdge(originVertex, destinationVertex, capacity);
            updatedVertices = updatedEdges = true; // Used for memoization
            return true;
        }
    }

    /**
     * Adds all edges to a LinkedList and returns this, no null
     * elements in the list.
     * @return a linked list containing all edges, null if graph is
     * empty
     */
    public LinkedList<FlowEdge> getAllEdges() {
        if (getNumberOfEdges() == 0) {
            // Empty graph
            return null;
        }

        LinkedList<FlowEdge> allEdges = new LinkedList<FlowEdge>();

        for (int i = 0; i < network.length; i++) {
            for (int j = 0; j < network[i].length; i++) {
                if (network[i][j] != null) {
                    allEdges.add(network[i][j]);
                }
            }
        }

        return allEdges;
    }

    /**
     * @return number of edges in this graph
     */
    public int getNumberOfEdges() {
        if (!this.updatedEdges) {
            // Memoization
            return this.numberOfEdges;
        }

        // Reset number of edges
        this.numberOfEdges = 0;

        for (int i = 0; i < network.length; i++) {
            for (int j = 0; j < network[i].length; j++) {
                if (network[i][j].isRealEdge()) {
                    numberOfEdges++;
                }
            }
        }

        this.updatedEdges = false; // No longer unknown number of edges
        return numberOfEdges;
    }

    /**
     * @return number of vertices is this graph
     */
    public int getNumberOfVertices() {
        if (!this.updatedVertices) {
            // Memoization
            return this.numberOfVertices;
        }

        // Reset number of vertices
        this.numberOfVertices = 0;

        // Temporary counter to avoid double counting
        HashSet<Integer> tempCounter = new HashSet<>(network.length);

        for (int i = 0; i < network.length; i++) {
            for (int j = 0; j < network[i].length; j++) {
                if (network[i][j].isRealEdge()) {
                    tempCounter.add(i);  // Origin vertice
                    tempCounter.add(j);  // Destination vertice
                }
            }
        }

        numberOfVertices = tempCounter.size();

        updatedVertices = false;   // No longer unknown number of vertices
        return numberOfVertices;
    }

    /**
     * m lines with m numbers each (a matrix) where the flow is
     * presented, in the same format as the capacities of the input
     * (vertical index is from, and horizontal is to).
     *
     * Example:
     *   0 3 1 0 0
     *   0 0 1 2 0
     *   0 0 0 0 3
     *   0 0 1 0 1
     *   0 0 0 0 0
     */
    public String getNetworkFlowInStringRepresentation(boolean useCalculatedFlow) {
	FlowEdge[][] flowNetwork = maximumFlowNetwork;

        if (useCalculatedFlow && maximumFlowNetwork == null) {
            // Not calculated yet
            return null;
        } else if (!useCalculatedFlow) {
	    flowNetwork = network;
	}

        StringBuilder sb = new StringBuilder();
        Formatter format = new Formatter(sb);

        String outputFormat = getRightFormatting(flowNetwork.length);

        for (int i = 0; i < flowNetwork.length; i++) {
            for (int j = 0; j < flowNetwork[i].length; j++) {
		int flow = flowNetwork[i][j].getFlow();

                if (j != SINK) {
		    if (niceOutput) {
			format.format(outputFormat, flow);
		    } else {
			// Same as testfiles given on page
			sb.append(flow + " ");
		    }
		} else {
                    // Without trailing whitespace
                    sb.append(flow);
                }
            }

            if (i != flowNetwork.length-1) {
                // Don't add newline to last line
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * @return right dimension for printing out the graph
     */
    private String getRightFormatting(int dimension) {
        if (dimension <= 10) {
            return "%-2d";
        } else if (dimension <= 100) {
            return "%-3d";
        } else {
            return "%-4d";
        }
    }


    class FlowEdge implements Comparable<FlowEdge> {
        private int originVertex;
        private int destinationVertex;
        private int capacity;
        private int flow;
        private boolean realEdge = true;

        FlowEdge(int originVertex, int destinationVertex, int capacity) {
            this.originVertex = originVertex;
            this.destinationVertex = destinationVertex;
            this.capacity = capacity;

            if (capacity == 0) {
                // Edges with capacity of zero is not added to this graph
                realEdge = false;
            }
        }

        FlowEdge(FlowEdge f) {
            this.originVertex = f.originVertex;
            this.destinationVertex = f.destinationVertex;
            this.capacity = f.capacity;
            this.flow = f.flow;
            this.realEdge = f.realEdge;
        }

        public int from() { return originVertex; }
        public int to() { return destinationVertex; }
        public int getCapacity() { return capacity; }
        public int getFlow() { return flow; }
        public boolean isRealEdge() { return realEdge; }

	/**
	 * If this vertex is connected to this graph, return the other
	 * vertex (on the other side of this edge), else quit the
	 * program with an error message.
	 */
	public int getOtherVertex(int vertex) {
	    exitProgramWhenWrongVertex(vertex);

	    return vertex == originVertex ? destinationVertex : originVertex;
	}
        /**
         * This method returns the remaning capasity on this edge in
         * the direction of the vertex given as parameter
         */
        public int getRemainingCapasityToVertex(int vertex) {
            if (vertex == destinationVertex) {
                // Forwards
                return capacity-flow;
            } else if (vertex == originVertex) {
                // Backwards
                return flow;
            } else {
                // Vertex not connected to edge:
                exitProgramWhenWrongVertex(vertex);
                return -1;
            }
        }

	/**
	 * @see getRemainingCapasityToVertex
	 */
        public int getRemainingCapasityFromVertex(int vertex) {
	    return getRemainingCapasityToVertex(getOtherVertex(vertex));
	}

        /**
         * Add flow to this edge. If the vertex is destination the
         * flow is added, if the vertex is origin it is subtracted. If
         * the capacity is exceeded nothing will be done and the
         * method will return false.
         */
        public boolean addFlowToEdgeByVertex(int vertex, int newFlow) {
            if (vertex == destinationVertex) {
                return addFlowToEdge(newFlow);
            } else if (vertex == originVertex) {
                return addFlowToEdge(newFlow * -1);
            } else {
                // Vertex not connected to edge:
                exitProgramWhenWrongVertex(vertex);
                return false;
            }
        }

        /**
         * Update with new flow if the new flow does not exceed
         * capasity.
         * @param newFlow new flow to be added
         * @return true if the flow was successfully added
         */
        private boolean addFlowToEdge(int newFlow) {
            int updatedFlow = flow + newFlow;
            if (updatedFlow > capacity || updatedFlow < 0) {
                // Flowing over capacity
                System.err.println("Trying to add more flow than capacity: " + newFlow);
                System.err.println(this.toString());
                return false;
            }

            // Everything is fine, update flow:
            flow = updatedFlow;
            return true;
        }

        /**
         * @return true if the vertex given as parameter is connected
         * to this edge
         */
        public boolean isConnectedToVertex(int vertex) {
            if (vertex == destinationVertex ||
                vertex == originVertex) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * If the vertex given as parameter is not connected to this
         * graph the method exits the program with an error message.
         */
        private void exitProgramWhenWrongVertex(int vertex) {
            if (!isConnectedToVertex(vertex)) {
                System.err.println("Vertex: " + vertex + " not connected to this graph " +
                                   this.toString());
                System.exit(1);
            }
        }

	public boolean equeals(FlowEdge e) {
	    return originVertex == e.originVertex && destinationVertex == e.destinationVertex;
	}

	public int compareTo(FlowEdge e) {
	    if (originVertex == e.originVertex) {
		return destinationVertex - e.destinationVertex;
	    } else {
		return originVertex - e.originVertex;
	    }
	}

        public String toString() {
            return String.format("%d -> %d (%d/%d)", originVertex, destinationVertex, flow, capacity);
        }

    }
}
